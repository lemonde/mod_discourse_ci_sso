# name: discourse_ci_sso
# about: CI SSO support for Discourse
# version: 0.1
# authors: Benoît GRUNENBERGER
# url: https://bitbucket.org/lemonde/mod_discourse_ci_sso.git

auth_provider :title => 'avec Courrier International',
              :authenticator => Auth::OpenIdAuthenticator.new('ci','http://www.courrierinternational.com/openid/provider', trusted: true),
              # :message => 'Authentification avec Courrier International (Assuez-vous que votre blockeur de pop up est désactivé)',
              :frame_width => 1000,   # the frame size used for the pop up window, overrides default
              :frame_height => 800
